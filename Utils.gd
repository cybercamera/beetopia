extends Node
#Global script

#var all_organisms = {} #Holds all of the organisms in the arena
#var all_game_data = {} #holds all the game data to be saved

signal generate_load_field

const FILE_NAME = "user://game-data.json"
const CONFIG_FILE_NAME = "user://game-config.json"

var total_bees: int = 0
var total_flowers: int = 0
var total_honey: int = 0
var total_lifecycles: int = 0
var paused: bool = false
var just_started_game: bool = true

# Config settings
var play_music : bool = true
var play_sound_effects : bool = true
var display_size : int = 0 #defaults to small screen

# Assign each bee a default genetic code, for pollen productivity
var bee_genes = {
	"Yellow" : "ABCDEFGHIJKLMNOP",
	"Orange" : "ABCDEFGHIJKLMNOP",
	"Green" : "ABCDEFGHIJKLMNOP",
	"Rainbow" : "ABCDEFGHIJKLMNOP", 
	"Skep" : "ABCDEFGHIJKLMNOP"
}

# Assign each flower a genetic tag for productivity calculations

var pollen_productivity_gene_mapping = {
	"A" : "BlueLarge", 
	"B" : "Yellow", 
	"C" : "BlueSmall", 
	"D" : "OrangeSmall", 
	"E" : "OrangeLarge", 
	"F" : "Red", 
	"G" : "Pink",  
	"H" : "Purple", 
	"I" : "RedRose", 
	"J" : "AloeVera", 
	"K" : "BlackRose", 
	"L" : "Bonsai", 
	"M" : "YellowPot", 
	"N" : "GreenTree", 
	"O" : "Lavender"
}

#var pollen_productivity_gene_mapping = {
#	"BlueLarge" : "A", 
#	"Yellow" : "B", 
#	"BlueSmall" : "C", 
#	"OrangeSmall" : "D", 
#	"OrangeLarge" : "E", 
#	"Red" : "F", 
#	"Pink" : "G",  
#	"Purple" : "H", 
#	"RedRose" : "I", 
#	"AloeVera" : "J", 
#	"BlackRose" : "K", 
#	"Bonsai" : "L", 
#	"YellowPot" : "M", 
#	"GreenTree" : "N", 
#	"Lavender"  : "O" 
#}



onready var GrassField = get_tree().get_root().get_node("GrassField")
onready var HUD = get_tree().get_root().get_node("GrassField/HUD")
onready var Flowerbeds = get_tree().get_root().get_node("GrassField/Flowerbeds")
onready var Hives = get_tree().get_root().get_node("GrassField/Hives")
onready var WateringCan = get_tree().get_root().get_node("GrassField/HUD/MouseCursorWateringCan")
onready var HoneyCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Honey")
onready var BeesCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Bees")
onready var FlowersCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Flowers")
onready var LifecyclesCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Lifecycles")
onready var LoadFieldGenerator = get_tree().get_root().get_node("GrassField/HUD/LoadFieldGenerator")

onready var load_field_scene = load("res://scenes/LoadFieldGenerator.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	load_config()
	
func load_config() -> void:
	var save_config = File.new()
	if not save_config.file_exists(CONFIG_FILE_NAME):
		return # Error! We don't have a save to load.
		
	# Let's pull in all of the saved game config
	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_config.open(CONFIG_FILE_NAME, File.READ)
	var config_data = parse_json(save_config.get_line())
	# Only if we have data, do we try and process it
	
	if config_data.size() > 0:
		play_music = config_data["play_music"]
		play_sound_effects = config_data["play_sound_effects"] 
		display_size = config_data["display_size"] 
		bee_genes = config_data["bee_genes"]
		
	save_config.close()

func load_game() -> void:
	var save_game = File.new()
	if not save_game.file_exists(FILE_NAME):
		return # Error! We don't have a save to load.
		
	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for i in save_nodes:
		i.queue_free()
		
	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(FILE_NAME, File.READ)
	
	# Now we first get the line which holds the game information data
	# Things like the honey and bee count
	var info_data = parse_json(save_game.get_line())
	# Only if we have data, do we try and process it
	if info_data.size() > 0:
		total_bees = info_data["total_bees"]
		total_flowers = info_data["total_flowers"]
		total_honey = info_data["total_honey"]
		total_lifecycles = info_data["total_lifecycles"]
		# And now we show these values
		BeesCounter.text = "Bees: " + str(Utils.total_bees)
		FlowersCounter.text = "Flowers: " + str(Utils.total_flowers)
		HoneyCounter.text = "Honey: " + str(Utils.total_honey)
		LifecyclesCounter.text = "Lifecycles: " + str(Utils.total_lifecycles)
	
	# Next, we get all of the nodes.	
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		
		# Firstly, we need to determine what kind of save object this is.
		
		match node_data["save_type"]:
			"flower_field":
				#this is a flower field. We have to call our loadflowergenerator helper via signal
				#print(str(node_data))
				
				#Make a temp load field for this flowerbed
				var tmp_load_field = load_field_scene.instance()
				#move it out of the visible way
				tmp_load_field.position = Vector2(-5000,-5000)
				#add to HUD
				HUD.add_child(tmp_load_field)
				#Process field imager
				#emit_signal("generate_load_field", node_data)
				tmp_load_field.generate_load_field(node_data)
				
			
			"hive":
				#It's a hive. We need to create the object and add it to the tree and set its properties
				var new_object = load(node_data["filename"]).instance()
				#get_node(node_data["parent"]).add_child(new_object)
				new_object.position = Vector2(node_data["pos_x"], node_data["pos_y"])
				
				# Now we set the remaining variables.
				for i in node_data.keys():
					if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
						continue
					new_object.set(i, node_data[i])
					
				get_node(node_data["parent"]).add_child(new_object)
			_:
				print("More data!")
		
	save_game.close()
	
	
func save_config() -> void:
	# Called when we need to save the config settings.
	var save_config = File.new()
	save_config.open(CONFIG_FILE_NAME, File.WRITE)
	
	var save_dict = {
		"play_music" : play_music,
		"play_sound_effects" : play_sound_effects,
		"display_size" : display_size,
		"bee_genes" : bee_genes
	}
	
	#Save this data as the first line
	save_config.store_line(to_json(save_dict))
	save_config.close()
	
	
# Go through everything in the persist category and ask them to return a
# dict of relevant variables.
func save_game():
	var save_game = File.new()
	save_game.open(FILE_NAME, File.WRITE)
	#First we write out the infoboard game data. 
	
	var save_dict = {
		"total_bees" : total_bees,
		"total_flowers" : total_flowers,
		"total_honey" : total_honey,
		"total_lifecycles" : total_lifecycles
	}
	
	#Save this data as the first line
	save_game.store_line(to_json(save_dict))
	
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for node in save_nodes:
		# Check the node is an instanced scene so it can be instanced again during load.
		if node.filename.empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
			
		# Check the node has a save function.
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
			
		# Call the node's save function.
		var node_data = node.call("save")

		# Store the save dictionary as a new line in the save file.
		save_game.store_line(to_json(node_data))
	save_game.close()
