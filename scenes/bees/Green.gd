extends AnimatedSprite

export var speed : int = 60 #default max speed

func _ready():
	self.stop()

func _on_VisibilityEnabler2D_screen_entered():
	self.play()

func _on_VisibilityEnabler2D_screen_exited():
	self.stop()
