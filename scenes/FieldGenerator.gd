extends Node2D

signal game_timer

var noise = OpenSimplexNoise.new()
var cursor_filename = "res://assets/sprites/Flowers/1Blue.png"
var k : int
var l : int


# Called when the node enters the scene tree for the first time.
func _ready():
	#Set listener for signal from field cursor to generate new field
	get_parent().connect("generate_field", self, "generate_field")
	randomize()
	# Configure noise generator
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.8
	

func add_flowers(field_type : String) -> void:
	#print("add_flowers()")
	
	var sample :float  = 0.0
	#Seed field noise
	noise.seed = randi()
	#Let's clear the field canvas for the next cycle
	for child in $ViewportContainer/FieldViewport/FieldCanvas.get_children():
		child.queue_free()
	for x in range(40, 450, 10):
		for y in range(40, 450, 10):
			sample = noise.get_noise_2d(x, y)
			if sample > 0.13 and sample < 0.15:
				#var flower_scene = load("res://scenes/FlowerField.tscn")
				var flower_sprite = Sprite.new()
				flower_sprite.scale = Vector2(0.5,0.5)
				flower_sprite.texture = load("res://assets/sprites/Flowers/" + field_type + ".png")
				#We need to create a random numner of instances of a that flower
				flower_sprite.position = Vector2(x+20, y+20)
				$ViewportContainer/FieldViewport/FieldCanvas.add_child(flower_sprite)
				Utils.total_flowers += 1
				
func generate_field(field_type :String) -> void:
	add_flowers(field_type)
	yield(get_tree(), "idle_frame")
	var old_clear_mode = $ViewportContainer/FieldViewport.get_clear_mode()
	$ViewportContainer/FieldViewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured.
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	
	# Retrieve the captured image.
	var img = $ViewportContainer/FieldViewport.get_texture().get_data()
	
	# restore the previous value, as some part wont redraw after...
	$ViewportContainer/FieldViewport.set_clear_mode(old_clear_mode)
	
	# Flip it on the y-axis (because it's flipped).
	img.flip_y()

	# copy to a new image, I need to create it first with the same size with create()
	var imgDest = Image.new()
	imgDest.create(512, 512, false, img.get_format())
	imgDest.blit_rect(img,  Rect2(Vector2(0,0), Vector2(512, 512)), Vector2.ZERO)

	# Create a texture for it.
	var tex = ImageTexture.new()
	tex.create_from_image(imgDest)

	# the texture can be assigned to a Sprite2D or a TextureRect
	var shader_scene = load("res://scenes/FlowerField.tscn")
	var flower_image_instance = shader_scene.instance()
	flower_image_instance.set_sprite_texture(tex)
	flower_image_instance.field_type = field_type
	Utils.Flowerbeds.add_child(flower_image_instance)
	#print("get_parent().get_viewport().get_mouse_position(): " + str(get_parent().position))
	flower_image_instance.position = get_parent().position
	flower_image_instance.position
	#print("flower_image_instance.position: " + str(flower_image_instance.position))
	#flower_image_instance.position = Utils.GrassField.get_mouse_position()


func _on_GameTimer_timeout():
	emit_signal("game_timer")
