extends CanvasLayer

signal hide_current_field_cursor
signal show_current_field_cursor
signal game_timer
signal pouring_water_relay


var mouse_field_cursor = preload("res://scenes/mouse_cursors/MouseCursorFields.tscn")
var mouse_watering_can_cursor = preload("res://scenes/mouse_cursors/MouseCursorWateringCan.tscn")
var mouse_hive_cursor = preload("res://scenes/mouse_cursors/MouseCursorHives.tscn")

var current_field_cursor

var bees_active_audio_stream = preload("res://assets/audio/bee_audio2.ogg")
var bees_inactive_audio_stream = preload("res://assets/audio/bee_audio1.ogg")

#Radial button variables
export var radius = 120
export var speed = 0.25


# Called when the node enters the scene tree for the first time.
func _ready():
	$Help.text = "Welcome to Beetopia!\n\n In this game, you will plant and grow flowerbeds, install beehives, keep bees and collect honey.\n\nTo plant flowerbeds, select from the range available in the gardening menu on the right. Then you can click onto the areas on the grassfield that you want to plant these flowerbeds. Ditto for the different kinds of hives.\n\n As the flowerbeds grow, you'll need to water them, using the watering can from the gardening menu; a small icon will let you know when you need to water them. To water, hold the watering can above and a bit to the right of the flowerbed, and click the left mouse button.\n\n Each hive attracts different kinds of bees. Each bee kind is able to make more honey from different combinations of flowerbeds. To change these combinations, alter that bee-type's genetic code from the Options menu. Once you have mature flowerbeds, the bees will come out to collect pollen, then make honey. Once the hive has a sufficient amount of honey, a small honey-jar icon will appear. You can harvest this honey by clicking on the hive.\n\n Enjoy your time in Beetopia!\n(PS: You can hide this Help screen by clicking on the Help button once more.)"
	$Help.visible = false
	music_player(Utils.play_music)
	sound_effects_player(Utils.play_sound_effects)
	# Hook into audio options settings
	$Options.connect("play_music", self, "music_player")
	$Options.connect("play_sound_effects", self, "sound_effects_player")
	if Utils.just_started_game:
		show_temp_message("Welcome to Beetopia!\nEnjoy your gardening and beekeeping!")
		Utils.just_started_game = false
	
#func _process(delta):
#	get_input()
#
#func get_input() -> void:
#	pass
#

func _on_ButtonQuit_pressed():
	get_tree().quit()

func enable_active_bee_sound_effects(bees_active : bool) -> void:
	# Depending on wether bees are out or not, witll switch audio FX tracks
	var stream_playing = $SoundEffectsPlayer.is_playing()
	#print("stream: " + $SoundEffectsPlayer.stream.resource_path.get_file())
	if bees_active:
		if $SoundEffectsPlayer.stream != bees_active_audio_stream:
			$SoundEffectsPlayer.stream = bees_active_audio_stream
			if stream_playing:
				# We've changed track. If the player was playing, make it play again
				$SoundEffectsPlayer.play()
				print("playing bee3")
	else:
		if $SoundEffectsPlayer.stream != bees_inactive_audio_stream:
			$SoundEffectsPlayer.stream = bees_inactive_audio_stream
			if stream_playing:
				# We've changed track. If the player was playing, make it play again
				$SoundEffectsPlayer.play()
				print("playing bee1")

func new_game() -> void:
	Utils.total_bees = 0
	Utils.total_flowers = 0
	Utils.total_honey = 0
	Utils.total_lifecycles = 0
	get_tree().change_scene("res://scenes/Grassfield.tscn")

func hide_message():
	$Information.visible = false
	$Information.text = ""

func mouse_entered() -> void:
	if  current_field_cursor and $MouseExitTimer.is_stopped():
		#print("emit signal")
		emit_signal("hide_current_field_cursor")

func mouse_exited() -> void:
	if  current_field_cursor:
		#print("emit signal")
		emit_signal("show_current_field_cursor")

func music_player(play_music : bool) -> void:
	# Plays the music track
	$MusicPlayer.playing = play_music
	#$MusicPlayer._set_playing(play_music) 

func pouring_water_relay() -> void:
	#when called, emits a signal to let all the flower fields know that water is being poured
	emit_signal("pouring_water_relay")

func show_help():
	$Information.visible = true

func show_message(message_text: String):
	$Information.text = message_text
	$Information.visible = true

func show_temp_message(message_text: String) -> void:
	show_message(message_text)
	yield(get_tree().create_timer(3.0), "timeout")
	hide_message()


func show_field_cursor(field_type : String) -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_field_cursor.instance()
		new_cursor.field_type_cursor = field_type
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.position = get_viewport().get_mouse_position()
		current_field_cursor = new_cursor
		$MouseExitTimer.start()

func show_hive_cursor(field_type : String) -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_hive_cursor.instance()
		new_cursor.hive_type_cursor = field_type
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.position = get_viewport().get_mouse_position()
		current_field_cursor = new_cursor
		$MouseExitTimer.start()

func sound_effects_player(play_sound_effects : bool) -> void:
	# Plays the music track
	$SoundEffectsPlayer.playing = play_sound_effects

func _on_Controls_mouse_entered():
	mouse_entered()


func _on_Controls_mouse_exited():
	mouse_exited()
	

func _on_GameTimer_timeout():
	emit_signal("game_timer")
	Utils.total_lifecycles += 1
	$Infobox/Bees.text = "Bees: " + str(Utils.total_bees)
	$Infobox/Flowers.text = "Flowers: " + str(Utils.total_flowers)
	$Infobox/Honey.text = "Honey: " + str(Utils.total_honey)
	$Infobox/Lifecycles.text = "Lifecycles: " + str(Utils.total_lifecycles)
	#Let's check to see if there are any bees active. If not, switch to the no-bee sound effects
	var bees_active : bool = false
	for hive_children in Utils.Hives.get_children():
		# for each hive
		if hive_children.get_child_count() > hive_children.core_child_count:
			# This hive has children beyond its core component children
			bees_active = true
			
	enable_active_bee_sound_effects(bees_active) # Used to alert the sound_effects_player to change bee sound status

func _on_ButtonNew_pressed():
	new_game()


func _on_ButtonSave_pressed():
	Utils.save_game()
	show_temp_message("Game Saved.")


func _on_ButtonLoad_pressed():
	Utils.load_game()
	show_temp_message("Game Loaded.")


func _on_ButtonPause_pressed():
	if Utils.paused: 
		#we're already paused, let's unpause
		hide_message()
		Utils.paused = not Utils.paused
		get_tree().set_pause(Utils.paused)
		
	else:
		#let's pause
		Utils.paused = not Utils.paused
		get_tree().set_pause(Utils.paused)
		show_message("Beetopia Paused!")


func _on_ButtonHelp_pressed():
	print(str($Help.visible))
	if $Help.visible:
		$Help.visible = false
	else:
		$Help.visible = true


func _on_FlowerPanel_mouse_entered():
	mouse_entered()

func _on_Infobox_mouse_entered():
	mouse_entered()

func _on_ButtonOptions_pressed():
	print("$Options.stop_processing: " + str($Options.stop_processing))
	if $Options.stop_processing:
		$Options.show()
		$Options.stop_processing = false
	else:
		$Options.hide()
		$Options.stop_processing = true

# Flower and hive panel buttons

func _on_ButtonBlueLarge_pressed():
	show_field_cursor("BlueLarge")


func _on_ButtonLavender_pressed():
	show_field_cursor("Lavender")


func _on_ButtonGreenTree_pressed():
	show_field_cursor("GreenTree")


func _on_ButtonYellowPot_pressed():
	show_field_cursor("YellowPot")


func _on_ButtonBonsai_pressed():
	show_field_cursor("Bonsai")


func _on_ButtonBlackRose_pressed():
	show_field_cursor("BlackRose")


func _on_ButtonAloeVera_pressed():
	show_field_cursor("AloeVera")


func _on_ButtonRedRose_pressed():
	show_field_cursor("RedRose")

func _on_ButtonYellow_pressed():
	show_field_cursor("Yellow")

func _on_ButtonBlueSmall_pressed():
	show_field_cursor("BlueSmall")

func _on_ButtonOrangeSmall_pressed():
	show_field_cursor("OrangeSmall")

func _on_ButtonOrangeLarge_pressed():
	show_field_cursor("OrangeLarge")

func _on_ButtonRed_pressed():
	show_field_cursor("Red")

func _on_ButtonPink_pressed():
	show_field_cursor("Pink")


func _on_ButtonPurple_pressed():
	show_field_cursor("Purple")


func _on_panel_button_mouse_entered():
	mouse_entered()

func _on_panel_button_mouse_exited():
	mouse_exited()



func _on_ButtonWateringCan_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_watering_can_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		get_parent().add_child(new_cursor) #Add to level node
		current_field_cursor = new_cursor
		new_cursor.connect("pouring_water", self, "pouring_water_relay")
		$MouseExitTimer.start()


func _on_ButtonRainbowHive_pressed():
	show_hive_cursor("Rainbow")

func _on_ButtonOrangeHive_pressed():
	show_hive_cursor("Orange")

func _on_ButtonGreenHive_pressed():
	show_hive_cursor("Green")

func _on_ButtonYellowHive_pressed():
	show_hive_cursor("Yellow")

func _on_ButtonSkepHive_pressed():
	show_hive_cursor("Skep")
