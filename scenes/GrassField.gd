extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	#We have to establish the various global node references, in case this node
	#has been respawned and the references to the previous lot are now lost
	Utils.GrassField = get_tree().get_root().get_node("GrassField")
	Utils.HUD = get_tree().get_root().get_node("GrassField/HUD")
	Utils.Flowerbeds = get_tree().get_root().get_node("GrassField/Flowerbeds")
	Utils.Hives = get_tree().get_root().get_node("GrassField/Hives")
	Utils.WateringCan = get_tree().get_root().get_node("GrassField/HUD/MouseCursorWateringCan")
	Utils.HoneyCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Honey")
	Utils.BeesCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Bees")
	Utils.FlowersCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Flowers")
	Utils.LifecyclesCounter = get_tree().get_root().get_node("GrassField/HUD/Infobox/Lifecycles")
	#Utils.LoadFieldGenerator = get_tree().get_root().get_node("GrassField/HUD/LoadFieldGenerator")
