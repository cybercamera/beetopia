extends Node2D

#When instanced, shows an image of the honey jar pulsating

func _ready():
	print("honeyjar pos: " + str(self.position))
	$Tween.interpolate_property(self, "position", self.position, Utils.HoneyCounter.rect_position + (Utils.HoneyCounter.rect_size/2), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	


func _on_Tween_tween_all_completed():
	#Jar has been harvested. Increment the honey counter
	Utils.total_honey += 1
	Utils.HoneyCounter.text = "Honey: " + str(Utils.total_honey)
	#Then free this node
	queue_free()
	
