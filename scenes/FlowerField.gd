extends Node2D

export var is_mature : bool = false
export var fade_level : float = 0.1
#var flowers_beds = {} #holds all of the current flower beds.
export(String, "BlueLarge", "Yellow", "BlueSmall", "OrangeSmall", "OrangeLarge", "Red", "Pink",  "Purple", "RedRose", "AloeVera", "BlackRose", "Bonsai", "YellowPot", "GreenTree", "Lavender" ) var field_type  = "Lavender"

const WATER_CYCLE_STATE = 0
const MATURITY_CYCLE_STATE = 1
const LIFE_CYCLE_STATE = 2
const WITHER_CYCLE_STATE = 3

var curr_cycle_state = WATER_CYCLE_STATE #we start with water state 

var field_mature : bool = false
var cycle_timer : int = 0
var water_count : int = 0
var cycles_since_last_watering : int = 0

#The following are the data params of the various flowers/plants
#The hold values like: 
#	how many cycles of watering is needed
#	how many cycles they take to become viable
#	how many cycles they live for 
#	how many cycles they take to wither and are yet still viable

var plant_params = {
	"BlueLarge": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Yellow": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"BlueSmall": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"OrangeSmall": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"OrangeLarge": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Red": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Pink": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Purple": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"RedRose": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"AloeVera": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"BlackRose": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Bonsai": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"YellowPot": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"GreenTree": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10},
	"Lavender": {"water_cycles":2, "maturity_cycles":10, "life_span":50, "wither_cycles":10}
	}

var was_field_watered : bool = false
var original_scale = Vector2(0,0)
var original_waterme_scale = Vector2(0,0)

func _ready():
	#get_parent().connect("game_timer", self, "game_timer_tick")
	Utils.HUD.connect("game_timer", self, "game_timer_tick")
	Utils.HUD.connect("pouring_water_relay", self, "check_if_watered")
	$Sprite.modulate = Color($Sprite.modulate.r, $Sprite.modulate.g, $Sprite.modulate.b, fade_level)
	original_scale = $Sprite.scale
	original_waterme_scale = $WaterMe.scale
	$WaterMe.visible = false
	add_to_group("Persist") #This is used for finding entities to save in the save file.
	

func check_if_watered() -> void:
	#Called when water relay has alerted us that water is being poured. Check to see if above us
	var distance_to_watering_can = self.position.distance_to(get_global_mouse_position()); 
	#print(str(distance_to_watering_can))
	#print(str(self.position), str(get_global_mouse_position()))
	if distance_to_watering_can > 150 and  distance_to_watering_can < 300: 
		#We're the right distance out. Are we in the right (top right) quadrant?
		var x_run = self.position.x - get_global_mouse_position().x
		var y_rise = self.position.y - get_global_mouse_position().y
		var angle_to_watering_can = atan2(y_rise, x_run) + PI
		if angle_to_watering_can > 4.9 and angle_to_watering_can < 5.6:
			field_watered()

func field_watered() -> void:
	#This func is called when we know we've been watered. A field womt grow until it has been watered.
	#when it's been watered, it pulses for a few seconds, to show off
	if !was_field_watered:
		#We only want to do this if we've not been watered already
		$Tween.interpolate_property($Sprite, "scale", $Sprite.scale, 1.1*original_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()
		was_field_watered = true
		$WaterMe.visible = false

func find_closest_flowerbed(hive_position: Vector2) -> Dictionary:
	#Given the location of a hive, will return the 5 closest flower beds
	
	var temp_sprite = Sprite.new()
	return temp_sprite

func game_timer_tick() -> void:
	#Much of the node logic lies here.
	#In this func, the node determines when the plant field should be watered, fade in, live, and how long to wither
	#We may have been loaded from a save file. if so, check to see if we need to set the fade level
	#print("field_type: " + field_type)
	cycle_timer += 1
	#First, we check to see if the field is mature. If it's not, then we're in a water cycle state
	if !field_mature:
		#We can only be here during a water state or a maturing state
		cycles_since_last_watering += 1
		match int(curr_cycle_state):
			WATER_CYCLE_STATE:
				#We are here because the field needs to be watered before it can progress to the next step
				if was_field_watered:
					#The field was watered, which means we have to reset that flag, and change the staae machine
					was_field_watered = false
					water_count += 1
					curr_cycle_state = MATURITY_CYCLE_STATE
					cycles_since_last_watering = 0
				else:
					#Let's check to see if the field has been sufficiently watered
					#we determine that by checking to see if the water_count is at least as big as
					#the field's specified water_cycles
					if water_count >= plant_params[field_type]["water_cycles"]:
						#We are here because we've had sufficient amounts of watering.
						#So change the state to MATURITY
						curr_cycle_state = MATURITY_CYCLE_STATE
					else:
						#We are here because there's not been enough watering done
						#We therefore show the water_me icon
						$WaterMe.visible = true
						$Tween.interpolate_property($WaterMe, "scale", $WaterMe.scale, 1.8*original_waterme_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
						$Tween.start()
						
						
			MATURITY_CYCLE_STATE:
				#We are here because the plant is growing. We need to ensure it gets watered appropriately
				if water_count < plant_params[field_type]["water_cycles"]:
					#We're here because we've not been watered fully
					var water_to_maturity_ratio = plant_params[field_type]["maturity_cycles"]/plant_params[field_type]["water_cycles"]
					if cycles_since_last_watering > (water_to_maturity_ratio):
						#We are here because we've reached another water cycle
						curr_cycle_state = WATER_CYCLE_STATE
			
				elif cycle_timer > plant_params[field_type]["maturity_cycles"]:
					#We are here because we've had enough watering and we're mature enough
					curr_cycle_state = LIFE_CYCLE_STATE
					self.is_mature = true
					#And let's reset the cycle timer
					cycle_timer = 0
					
				#And we need to reveal the plant
				if fade_level < 1.0:
					fade_level += 0.1
					$Tween.interpolate_property($Sprite, "modulate", $Sprite.modulate, Color($Sprite.modulate.r, $Sprite.modulate.g, $Sprite.modulate.b, fade_level), 9.95, Tween.TRANS_QUART, Tween.EASE_OUT)
					$Tween.start()
			
			
			LIFE_CYCLE_STATE:
				#We are here because we're in our standard life-cycle. 
				if cycle_timer > plant_params[field_type]["life_span"]:
					#We are here because we're now old enough to wither
					curr_cycle_state = WITHER_CYCLE_STATE
					#And let's reset the cycle timer
					cycle_timer = 0
					
			WITHER_CYCLE_STATE:
				if cycle_timer > plant_params[field_type]["wither_cycles"]:
					queue_free()
				
				if fade_level > 0.1:
					fade_level -= 0.1
					$Tween.interpolate_property($Sprite, "modulate", $Sprite.modulate, Color($Sprite.modulate.r, $Sprite.modulate.g, $Sprite.modulate.b, fade_level), 9.95, Tween.TRANS_QUART, Tween.EASE_OUT)
					$Tween.start()


func save():
	var save_dict = {
		"save_type" : "flower_field",
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"is_mature" : is_mature,
		"fade_level" : fade_level,
		"field_type" : field_type,
		"curr_cycle_state" : curr_cycle_state,
		"field_mature" : field_mature,
		"cycle_timer" : cycle_timer,
		"water_count" : water_count,
		"cycles_since_last_watering" : cycles_since_last_watering
	}
	#print(str(save_dict))
	return save_dict

func set_sprite_scale(new_scale : Vector2):
	$Sprite.scale = new_scale

func set_sprite_texture(new_texture : ImageTexture):
	$Sprite.texture = new_texture

func watered() -> void:
	#Called when watering is happening. Need to check to see if we're within the area of the water droplets
	print(str(get_local_mouse_position()))

func _on_VisibilityEnabler2D_screen_entered():
	#print("_on_VisibilityEnabler2D_screen_entered()")
	self.use_parent_material = false

func _on_VisibilityEnabler2D_screen_exited():
	#print("_on_VisibilityEnabler2D_screen_exited()")
	self.use_parent_material = true


func _on_Tween_tween_all_completed():
	if $WaterMe.scale > original_waterme_scale:
		#if we're too big, revert back to standard
		$Tween.interpolate_property($WaterMe, "scale", $WaterMe.scale, original_waterme_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()
		
	if $Sprite.scale > original_scale:
		#if we're too big, revert back to standard
		$Tween.interpolate_property($Sprite, "scale", $Sprite.scale, original_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()
