extends Node2D

#This class spawns and manages the bee paths, ensuring that bee paths take the bees approximately to the flowerbeds


export var max_beepaths_for_hive : int = 15 #How many beepaths in total can be spawned 
export var max_beepath_lifespan : int = 25 #How many game timer cycles before bees retire
export var max_distance_from_hive : int = 600 #How many pixels away do the paths go
export var core_child_count : int = 0

export(String, "Yellow", "Orange", "Green", "Rainbow", "Skep") var hive_type  = "Yellow"

onready var bee_spawn_audio = preload("res://assets/audio/bee_audio3.ogg")

var honey_jar_harvest = preload("res://scenes/HoneyJar.tscn")

const HONEY_JAR_SIZE = 1000 #How much honey does it take to fill the honey jar

var next_flowerbed_to_visit : int = 0 #Sspecifies which flowerbed we will spawn a beepath for
#var curr_beepath_count : int = 0 #Specifies how many bees are active now.

var randomisation_mod : int = 0 #This helps to randomise when new bees are released
var prime_array = [3, 5, 7, 11, 13] #Primes to be selected for bee release timing

var honey_total : int = 0 #Holds the amount of accumulated honey produced

# The following holds the default values for plant pollen
var plant_pollen = {
	"BlueLarge": 20,
	"Yellow": 20, 
	"BlueSmall": 10,
	"OrangeSmall": 10,
	"OrangeLarge": 10,
	"Red": 10,
	"Pink": 10,
	"Purple": 10,
	"RedRose": 10,
	"AloeVera": 10,
	"BlackRose": 10,
	"Bonsai": 10,
	"YellowPot": 20,
	"GreenTree": 10,
	"Lavender": 20
	}

# The following holds the pollen productivy matrix for the various bees
var bee_productivity_mapping = {
	"Yellow" : {},
	"Orange" : {},
	"Green" : {},
	"Rainbow" : {},
	"Skep" : {}
	}

var original_scale = Vector2(0,0)
var original_honeyjar_scale = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	core_child_count = self.get_child_count()
	Utils.HUD.connect("game_timer", self, "game_timer_tick")
	randomisation_mod = prime_array[randi()%5]
	original_scale = $HiveSprite.rect_scale
	original_honeyjar_scale = $HoneyJar.scale
	$HoneyJar.visible = false
	set_sprite_texture("res://assets/sprites/hives/" + hive_type + ".png") 
	add_to_group("Persist") #This is used for finding entities to save in the save file.
	establish_bee_pollen_productivity_mapping()

func compute_honey_production() -> void:
	#This gets called to compute how much honey can be/has been captured by the bees in this hive.
	#The parameters are:
	#1) What kind of bees they are, which can impact pollen collection based on genetic params
	#2) How many bees are active
	#3) How many flowerbeds within our capture area, and how far away they are. The closer, the more round trips that can be made, thus more honey
	#4) What kind of flowerbeds they are, as this meshes with the bee genetics to make some beds much more productive than others
	var curr_beepath_count = self.get_child_count() - 1 #Determine number of kids; subtract the core child nodes
	#First, check to see if we're still below the hive spawn limit for the beepaths
	var local_flowers = find_local_flowerbeds(self.position)
	#We have the closest flowers dictionary. Let's extract the keys and only count the ones in range
	#for flower_bed_dist in local_flowers.keys():
	for flower_bed_dist in local_flowers.keys():
		#For each key (i.e, distance) from the flower bed dictionary
		if flower_bed_dist < max_distance_from_hive:
			#Bed is within range
			#print("Flower bed position. " + str(local_flowers[flower_bed_dist]["position"]))
			#print("Flower type. " + str(local_flowers[flower_bed_dist]["field_type"]))
			honey_total += plant_pollen[local_flowers[flower_bed_dist]["field_type"]] * 600/flower_bed_dist
			#print(str(honey_total))
			
func distance_custom_sort(a:float, b:float)  -> bool:
	if a < b:
		return true
	else:
		return false

func establish_bee_pollen_productivity_mapping() -> void:
	# Will generate a mapping for this hive's bees' pollen collection for the various plants
	# What we want to do is get the genetic code for this bee, and iterate through it in order, assigning 
	# top value to the first string, less value to the next, etc.
	var productivity_value : int = 20
	for c in Utils.bee_genes[hive_type]:
		# For each char in this bee types genetic code
		plant_pollen[Utils.pollen_productivity_gene_mapping[c]] = productivity_value
		productivity_value -= 1
	print(str(plant_pollen))

func find_closest_flowerbed(hive_position: Vector2) -> Dictionary:
	#Given the location of a hive, will find the closest flower beds
	var closest_flower_beds = {}
	for flower_bed in Utils.Flowerbeds.get_children():
		#Let's assume that the flower beds are a flat structure, i.e, no children
		#and let's check to see if the next flowerbed is mature
		if flower_bed.is_mature:
			closest_flower_beds[self.position.distance_to(flower_bed.position)] = flower_bed.position
	
	return closest_flower_beds

func find_local_flowerbeds(hive_position: Vector2) -> Dictionary:
	#Given the location of a hive, will find the local flower beds within its bees' range
	var local_flower_beds = {}
	for flower_bed in Utils.Flowerbeds.get_children():
		#Let's assume that the flower beds are a flat structure, i.e, no children
		#and let's check to see if the next flowerbed is mature
		if flower_bed.is_mature:
			var flower_bed_attributes = {}
			flower_bed_attributes["position"] = flower_bed.position
			flower_bed_attributes["field_type"] = flower_bed.field_type
			local_flower_beds[self.position.distance_to(flower_bed.position)] = flower_bed_attributes
	
	return local_flower_beds

func game_timer_tick() -> void:
	#In this routine, we check to see where all the yummy flowers are at. 
	#We then point the path towards them
	#First, check to see if this is our turn to release a bee
	
	#print("total_lifecycles: " + str(Utils.total_lifecycles), " rnd: " + str(randomisation_mod))
	if Utils.total_lifecycles % randomisation_mod == 0:
		#We are due for a bee release then
		spawn_bee_path()
		
	# We now have to determine how much honey we can collect, if the bees are active
	if self.get_child_count() > 0:
		compute_honey_production()
	
	check_honey_jar_ready()
	
#func set_sprite_texture(new_texture : ImageTexture) -> void:
#	$Sprite.texture = new_texture

func check_honey_jar_ready() -> void:
	if honey_total > HONEY_JAR_SIZE:
		$HoneyJar.visible = true
		$Tween.interpolate_property($HoneyJar, "scale", $HoneyJar.scale, 2.5*original_honeyjar_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()

func save():
	var save_dict = {
		"save_type" : "hive",
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"hive_type" : hive_type,
		"next_flowerbed_to_visit" : next_flowerbed_to_visit,
		"randomisation_mod" : randomisation_mod,
		"honey_total" : honey_total
	}
	return save_dict

func set_sprite_texture(new_texture : String) -> void:
	$HiveSprite.texture = load(new_texture)

func spawn_bee_path():
	spawn_bee_sound()
	var curr_beepath_count = self.get_child_count() - core_child_count #Determine number of kids; subtract the core child nodes
	#First, check to see if we're still below the hive spawn limit for the beepaths
	if curr_beepath_count <= max_beepaths_for_hive:
		#We are here because we can yet spawn more bees
		var closest_flowers = find_closest_flowerbed(self.position)
		#We have the closest flowers dictionary. Let's extract the keys and sort them on distance.
		
		var closest_flower_dist = []	
		for dist in closest_flowers.keys():
			closest_flower_dist.append(dist)
		
		closest_flower_dist.sort()
		#We want to spawn a new beepath in the direction of the nextclosest flowerbed.
		#Which beepath we spawn will depend on the distance to that flowerbed.
		#For now, as we only have one beepath, we use that.
		#Let's attempt to spawn a new beepath for the next flowerbed
		
		if closest_flower_dist.size() > next_flowerbed_to_visit:
			#First, let's check to see if the next flowerbed is less than our max travel distance
			if closest_flower_dist[next_flowerbed_to_visit] < max_distance_from_hive:
				var bee_path_scene 
				if closest_flower_dist[next_flowerbed_to_visit] > 500:
					bee_path_scene = load("res://scenes/bee_paths/BeePath5.tscn").instance()
				elif  closest_flower_dist[next_flowerbed_to_visit] > 400:
					bee_path_scene = load("res://scenes/bee_paths/BeePath4.tscn").instance()
				elif  closest_flower_dist[next_flowerbed_to_visit] > 300:
					bee_path_scene = load("res://scenes/bee_paths/BeePath3.tscn").instance()
				elif  closest_flower_dist[next_flowerbed_to_visit] > 200:
					bee_path_scene = load("res://scenes/bee_paths/BeePath2.tscn").instance()
				else:
					bee_path_scene = load("res://scenes/bee_paths/BeePath1.tscn").instance()
				
				bee_path_scene.hive_type = self.hive_type #Set the kind of bee this is.
				var x_run = self.position.x - closest_flowers[closest_flower_dist[next_flowerbed_to_visit]].x
				var y_rise = self.position.y - closest_flowers[closest_flower_dist[next_flowerbed_to_visit]].y
				#print("y_rise: " + str(y_rise))
				#print(str("bee_path_scene.rotationC: " + str(rad2deg(atan2(y_rise, x_run)))))
				bee_path_scene.rotation = atan2(y_rise, x_run) + PI
				#print("Bee to: " + str(closest_flower_dist[next_flowerbed_to_visit]))
				self.add_child(bee_path_scene)
			next_flowerbed_to_visit += 1
			Utils.total_bees += 1
		else:
			#Reset the counter back to zero
			next_flowerbed_to_visit = 0

func spawn_bee_sound() -> void:
	# Called each time there's a new bee spawned
	# We only play this sound if sound effects are on
	if Utils.play_sound_effects:
		$BeeSound.play()

func _on_Tween_tween_all_completed():
	if $HoneyJar.scale > original_honeyjar_scale:
		#if we're too big, revert back to standard
		$Tween.interpolate_property($HoneyJar, "scale", $HoneyJar.scale, original_honeyjar_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()
		
	if $HiveSprite.rect_scale > original_scale:
		#if we're too big, revert back to standard
		$Tween.interpolate_property($HiveSprite, "rect_scale", $HiveSprite.rect_scale, original_scale, 0.2, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.start()


func _on_HiveSprite_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			if honey_total > HONEY_JAR_SIZE:
				#We need to harvest one jar of honey. We do that by hiding the small honey jar icon
				#and sending that jar of honey up to the honey counter
				$HoneyJar.visible = false
				#we reduce the homey total by that jar amount's worth
				honey_total -= HONEY_JAR_SIZE
				var honey_harvest_instance = honey_jar_harvest.instance()
				print("mouse pos: " + str(get_viewport().get_mouse_position()))
				honey_harvest_instance.global_position = get_viewport().get_mouse_position()
				Utils.HUD.add_child(honey_harvest_instance) #Add to hud node
				if Utils.play_sound_effects:
					$HoneyJarHarvestSound.play()


func _on_VisibilityNotifier2D_screen_entered():
	# We're visible, so unpause stream, if paused
	$BeeSound.stream_paused = false


func _on_VisibilityNotifier2D_screen_exited():
	# We're not visible, so pause stream, if unpaused
	$BeeSound.stream_paused = true
