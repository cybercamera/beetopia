extends Container

signal play_music
signal play_sound_effects

export var stop_processing: bool = true

var bee_gene_code = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"] 

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	add_option_items()
	$ScrollContainer/VerticalList/PlaySoundEffects.pressed = Utils.play_sound_effects
	$ScrollContainer/VerticalList/PlayMusic.pressed = Utils.play_music
	_on_DisplaySize_item_selected(Utils.display_size)
	$ScrollContainer/VerticalList/YellowBeeGene/YellowBeeLineEdit.text = Utils.bee_genes["Yellow"]
	$ScrollContainer/VerticalList/OrangeBeeGene/OrangeBeeLineEdit.text = Utils.bee_genes["Orange"]
	$ScrollContainer/VerticalList/GreenBeeGene/GreenBeeLineEdit.text = Utils.bee_genes["Green"]
	$ScrollContainer/VerticalList/RainbowBeeGene/RainbowBeeLineEdit.text = Utils.bee_genes["Rainbow"]
	$ScrollContainer/VerticalList/SkepBeeGene/SkepBeeLineEdit.text = Utils.bee_genes["Skep"]

func _process(delta):
	if !stop_processing:
		if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_cancel"):
			hide_options()
			
func add_option_items() -> void:
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1024 x 630")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1440 x 900")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1920 x 1024")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: Full Size")
	
func gen_gene() -> String:
	bee_gene_code.shuffle()
	var s : String
	for t in bee_gene_code:
		s = s + t
	return s
	
func get_config_values() -> void:
	Utils.bee_genes["Yellow"] = $ScrollContainer/VerticalList/YellowBeeGene/YellowBeeLineEdit.text
	Utils.bee_genes["Orange"] = $ScrollContainer/VerticalList/OrangeBeeGene/OrangeBeeLineEdit.text
	Utils.bee_genes["Green"] = $ScrollContainer/VerticalList/GreenBeeGene/GreenBeeLineEdit.text
	Utils.bee_genes["Rainbow"] = $ScrollContainer/VerticalList/RainbowBeeGene/RainbowBeeLineEdit.text
	Utils.bee_genes["Skep"] = $ScrollContainer/VerticalList/SkepBeeGene/SkepBeeLineEdit.text
	
func hide_options():
	self.hide()
	stop_processing = true
	

func _on_Close_pressed():
	get_config_values()
	Utils.save_config()
	hide_options()


func _on_DisplaySize_item_selected(index):
	#Set the screen size based on option selected by user
	match index:
		0:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1024, 630))
		1:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1440, 900))
		2:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1920, 1024))
		3:
			OS.set_window_fullscreen(true)
		_:
			# If nothing matches, run this block of code
			pass
	
	# Set the screen-size config
	Utils.display_size = index
	

func _on_PlaySoundEffects_toggled(button_pressed):
	Utils.play_sound_effects = $ScrollContainer/VerticalList/PlaySoundEffects.pressed
	emit_signal("play_sound_effects", Utils.play_sound_effects)

func _on_PlayMusic_toggled(button_pressed):
	Utils.play_music = $ScrollContainer/VerticalList/PlayMusic.pressed
	emit_signal("play_music", Utils.play_music)

func _on_YellowBeeButton_pressed():
	$ScrollContainer/VerticalList/YellowBeeGene/YellowBeeLineEdit.text = gen_gene()

func _on_OrangeBeeButton_pressed():
	$ScrollContainer/VerticalList/OrangeBeeGene/OrangeBeeLineEdit.text = gen_gene()

func _on_GreenBeeButton_pressed():
	$ScrollContainer/VerticalList/GreenBeeGene/GreenBeeLineEdit.text = gen_gene()

func _on_RainbowBeeButton_pressed():
	$ScrollContainer/VerticalList/RainbowBeeGene/RainbowBeeLineEdit.text = gen_gene()

func _on_SkepBeeButton_pressed():
	$ScrollContainer/VerticalList/SkepBeeGene/SkepBeeLineEdit.text = gen_gene()

