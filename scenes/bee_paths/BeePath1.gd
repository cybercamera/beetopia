extends Path2D

export(String, "Yellow", "Orange", "Green", "Rainbow", "Skep") var hive_type  = "Yellow"
var ttl : int = 5
var bee_speed : int = 60
func _ready():
	#Set the bee type
	var bee_scene = load("res://scenes/bees/" + hive_type + ".tscn").instance()
	$BeeFollow.add_child(bee_scene)
	bee_scene.play()
	bee_speed = bee_scene.speed
	#$BeeFollow/Honeybee.play()
	
	#Hook the timer function into the game timer
	Utils.HUD.connect("game_timer", self, "game_timer_tick")

func _process(delta):
	$BeeFollow.offset += bee_speed * delta
	if ttl <= 0 and $BeeFollow.unit_offset < 0.01:
		#Once we've lived our life, and are back at the hive, free us
		queue_free()

	
func game_timer_tick() -> void:
	#If we've been alive for 30 seconds, free node.
	ttl -= 1
