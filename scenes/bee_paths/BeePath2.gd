extends Path2D

export(String, "Yellow", "Orange", "Green", "Rainbow", "Skep") var hive_type  = "Yellow"
var ttl : int = 5

func _ready():
	$BeeFollow/Honeybee.play()
	#Let's establish the rotation of this beepath, randomly, to add variety.
	randomize()
	Utils.HUD.connect("game_timer", self, "game_timer_tick")
	#let's establish a random direction for the direction of the bee path for now
	#rotation = rand_range(PI, -PI)
	#$BeeFollow.unit_offset = rand_range(0, 0.5)

func _process(delta):
	$BeeFollow.offset += $BeeFollow/Honeybee.speed * delta
	if ttl <= 0 and $BeeFollow.unit_offset < 0.01:
		#Once we've lived our life, and are back at the hive, free us
		queue_free()

	
func game_timer_tick() -> void:
	#If we've been alive for 30 seconds, free node.
	ttl -= 1
