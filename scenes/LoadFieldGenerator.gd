extends Node2D

#This code is used for reloading fields from a save file. As such, it's called by signal from the load_file() code
#signal game_timer

var noise = OpenSimplexNoise.new()
var k : int
var l : int


# Called when the node enters the scene tree for the first time.
func _ready():
	#Set listener for signal from field cursor to generate new field
	#get_parent().connect("generate_load_field", self, "generate_load_field")
	#Connect to the signal from the Utils node.
	get_node("/root/Utils").connect("generate_load_field", self, "generate_load_field")
	
	randomize()
	# Configure noise generator
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.8
	

func add_flowers(field_type : String) -> void:
	print("add_flowers():" + str(field_type))
	
	var sample :float  = 0.0
	#Seed field noise
	noise.seed = randi()
	#Let's clear the field canvas for the next cycle
	for child in $ViewportContainer/FieldViewport/FieldCanvas.get_children():
		child.queue_free()
	for x in range(40, 450, 10):
		for y in range(40, 450, 10):
			sample = noise.get_noise_2d(x, y)
			if sample > 0.13 and sample < 0.15:
				#var flower_scene = load("res://scenes/FlowerField.tscn")
				var flower_sprite = Sprite.new()
				flower_sprite.scale = Vector2(0.5,0.5)
				flower_sprite.texture = load("res://assets/sprites/Flowers/" + field_type + ".png")
				#We need to create a random numner of instances of a that flower
				flower_sprite.position = Vector2(x+20, y+20)
				$ViewportContainer/FieldViewport/FieldCanvas.add_child(flower_sprite)
				Utils.total_flowers += 1
				
func generate_load_field(saved_node) -> void:
	add_flowers(saved_node["field_type"])
	#print("generate_load_field()" + saved_node["field_type"])
	yield(get_tree(), "idle_frame")
	var old_clear_mode = $ViewportContainer/FieldViewport.get_clear_mode()
	$ViewportContainer/FieldViewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured.
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	
	# Retrieve the captured image.
	var img = $ViewportContainer/FieldViewport.get_texture().get_data()
	
	# restore the previous value, as some part wont redraw after...
	$ViewportContainer/FieldViewport.set_clear_mode(old_clear_mode)
	
	# Flip it on the y-axis (because it's flipped).
	img.flip_y()

	# copy to a new image, I need to create it first with the same size with create()
	var imgDest = Image.new()
	imgDest.create(512, 512, false, img.get_format())
	imgDest.blit_rect(img,  Rect2(Vector2(0,0), Vector2(512, 512)), Vector2.ZERO)

	# Create a texture for it.
	var tex = ImageTexture.new()
	tex.create_from_image(imgDest)

	# the texture can be assigned to a Sprite2D or a TextureRect
	var shader_scene = load("res://scenes/FlowerField.tscn")
	var flower_image_instance = shader_scene.instance()
	#flower_image_instance.set_sprite_texture(tex)
	flower_image_instance.get_node("Sprite").texture = tex
	print(str(tex), saved_node["field_type"])
	#flower_image_instance.field_type = saved_node["field_type"]
	#Now, let's iterate our way through the saved data and set these up on this new node.
	flower_image_instance.position = Vector2(saved_node["pos_x"], saved_node["pos_y"])
	# Now we set the remaining variables.
	#print(str(saved_node))
	for i in saved_node.keys():
		if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
			continue
			
		#We have to set the fade level manually
		if i == "fade_level":
			flower_image_instance.get_node("Sprite").modulate.a = saved_node["fade_level"]
			
		#Set the rest of the values
		flower_image_instance.set(i, saved_node[i])
	
	for i in Utils.Flowerbeds.get_children():
		#print(str(i.field_type)) 
		pass
		#print("flower_image_instance.set(" + str(i) + " saved_node[" + str(saved_node[i]) + "]")
	
	Utils.Flowerbeds.add_child(flower_image_instance)
#func _on_GameTimer_timeout():
#	emit_signal("game_timer")
