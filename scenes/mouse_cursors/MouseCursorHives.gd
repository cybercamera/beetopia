extends Node2D

#signal generate_field

export(String, "Yellow", "Orange", "Green", "Rainbow", "Skep") var hive_type_cursor  = "Yellow"

var click_timer_active: bool = false
var hive_cursor_active: bool = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	#Hook up to the HUD to get signals of cursor moving over panel
	var HUD = get_parent().get_node("HUD")
	$HiveImage.texture = load("res://assets/sprites/hives/" + hive_type_cursor + ".png")
	if HUD:
			HUD.connect("hide_current_field_cursor", self, "hide_hive_cursor")
			HUD.connect("show_current_field_cursor", self, "show_hive_cursor")

func _process(delta):
	self.position = self.get_global_mouse_position()
	get_input()
	
func clear_hive_cursor() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	queue_free()
		
func get_input():
	if !click_timer_active and (Input.is_action_pressed("ui_mouse_left_click") or Input.is_action_pressed("ui_accept")):
		print("active mouse click")
		click_timer_active = true
		$ClickTimer.start()
		spawn_hive()
	if Input.is_action_pressed("ui_cancel"):
		clear_hive_cursor()
		

func hide_hive_cursor() -> void:
	#We should only be able to clear the field cursor when the cursor has moved over the panel for the first time
	#But only after it has left the panel for the first time
	#print("hide_field_cursor()")
	if hive_cursor_active:
		clear_hive_cursor()
		hive_cursor_active = false

func show_hive_cursor() -> void:
	#We should only be able to clear the field cursor when the cursor has moved over the panel for the first time
	#print("show_field_cursor()")
	hive_cursor_active = true
	
	
func init():
	#This is called after the scene is instanced 
	if get_parent().is_a_parent_of(self):
		var cursor_filename = "res://assets/sprites/hives/" + hive_type_cursor + ".png"
		$HiveImage.texture = load(cursor_filename)

func spawn_hive() -> void:
	#We need to create an instance of a the current hive type and attach it to the parent level 
	#print("field_type_cursor: " + field_type_cursor)
	var hive_scene = load("res://scenes/Hive.tscn")
	var hive_instance = hive_scene.instance()
	hive_instance.position = self.get_global_mouse_position()
	hive_instance.hive_type = hive_type_cursor
	var cursor_filename = "res://assets/sprites/hives/" + hive_type_cursor + ".png"
#		print(cursor_filename)
#		var tex = load(cursor_filename)
	hive_instance.set_sprite_texture(cursor_filename)
	Utils.Hives.add_child(hive_instance) #Add to grassfield node
	if Utils.play_sound_effects:
		$HiveSound.play()
	
	#emit_signal("generate_field", field_type_cursor)

func _on_ClickTimer_timeout():
	click_timer_active = false
