extends Node2D

signal generate_field

export(String, "BlueLarge", "Yellow", "BlueSmall", "OrangeSmall", "OrangeLarge", "Red", "Pink",  "Purple", "RedRose", "AloeVera", "BlackRose", "Bonsai", "YellowPot", "GreenTree", "Lavender" ) var field_type_cursor  = "BlueLarge"

var click_timer_active: bool = false
var field_cursor_active: bool = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	#Hook up to the HUD to get signals of cursor moving over panel
	var HUD = get_parent().get_node("HUD")
	$FlowerImage.texture = load("res://assets/sprites/Flowers/" + field_type_cursor + ".png")
	if HUD:
			HUD.connect("hide_current_field_cursor", self, "hide_field_cursor")
			HUD.connect("show_current_field_cursor", self, "show_field_cursor")

func _process(delta):
	self.position = self.get_global_mouse_position()
	get_input()
	
func clear_field_cursor() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	queue_free()
		
func get_input():
	if !click_timer_active and (Input.is_action_pressed("ui_mouse_left_click") or Input.is_action_pressed("ui_accept")):
		print("active mouse click")
		click_timer_active = true
		$ClickTimer.start()
		spawn_field()
	if Input.is_action_pressed("ui_cancel"):
		clear_field_cursor()
		

func hide_field_cursor() -> void:
	#We should only be able to clear the field cursor when the cursor has moved over the panel for the first time
	#But only after it has left the panel for the first time
	print("hide_field_cursor()")
	if field_cursor_active:
		clear_field_cursor()
		field_cursor_active = false

func show_field_cursor() -> void:
	#We should only be able to clear the field cursor when the cursor has moved over the panel for the first time
	print("show_field_cursor()")
	field_cursor_active = true
	
	
func init():
	#This is called after the scene is instanced 
	if get_parent().is_a_parent_of(self):
		var cursor_filename = "res://assets/sprites/Fields/" + field_type_cursor + ".png"
		#var cursor_filename = "res://assets/sprites/Fields/1Blue_field.png"
		print(cursor_filename)
		$FlowerImage.texture = load(cursor_filename)

func spawn_field() -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a the current field type and attach it to the parent level 
		#print("field_type_cursor: " + field_type_cursor)
		emit_signal("generate_field", field_type_cursor)
		# We only play sound if effects are enabled
		if Utils.play_sound_effects:
			$PlantSound.play()

func _on_ClickTimer_timeout():
	click_timer_active = false
