extends Node2D

signal pouring_water


var pour_timer_active: bool = false
var mouse_cursor_active: bool = false


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	mouse_cursor_active = true
	pour_water(false)
	#Hook up to the HUD to get signals of cursor moving over panel
	
	#print("HUD node: " + str(HUD))
	if Utils.HUD:
			Utils.HUD.connect("hide_current_field_cursor", self, "hide_mouse_cursor")
			Utils.HUD.connect("show_current_field_cursor", self, "show_mouse_cursor")
			

func _process(delta):
	self.position = self.get_global_mouse_position()
	get_input()
	
func clear_mouse_cursor() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	queue_free()
		
func get_input():
	if !pour_timer_active and (Input.is_action_pressed("ui_mouse_left_click") or Input.is_action_pressed("ui_accept")):
		#print("active mouse click")
		pour_timer_active = true
		$PourTimer.start()
		pour_water(true)
		
	if Input.is_action_pressed("ui_cancel"):
		clear_mouse_cursor()
		

func hide_mouse_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	#But only after it has left the panel for the first time
	#print("hide_mouse_cursor()")
	if mouse_cursor_active:
		clear_mouse_cursor()
		mouse_cursor_active = false

func show_mouse_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	#print("show_mouse_cursor()")
	mouse_cursor_active = true
	

func pour_water(pour : bool) -> void:
	#When called with true, will switch on the water pouring
	$WaterSpout/Particles2D.emitting = pour
	if pour:
		emit_signal("pouring_water")
		#print("pouring water")
		if Utils.play_sound_effects:
			$WateringSound.play()

func _on_PourTimer_timeout():
	# First time throuygh, we stop the water
	if pour_timer_active:
		pour_timer_active = false
		pour_water(false)
		if Utils.play_sound_effects:
			# Only continue with this timer action if ws're actually playing sound effects
			$PourTimer.start()
	else:
		# Second time through, we stop the sound effects.
		$WateringSound.stop()


